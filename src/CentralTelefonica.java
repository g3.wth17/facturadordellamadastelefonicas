import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class CentralTelefonica {
	private ArrayList<Cliente> clientesRegistrados = new ArrayList<Cliente>();
	private ArrayList<CDR> registrosTelefonicos = new ArrayList<CDR>();
	private FactoriaPlan factoria = new FactoriaPlan();
	
	public ArrayList<CDR> getRegistrosTelefonicos(){
		return registrosTelefonicos;
	}
	
	public void registrarNuevoClientePlanNormal(Cliente cliente, String planCliente) {
		IPlan nuevoPlan;
		nuevoPlan = factoria.getPlan(planCliente);
		cliente.setPlan(nuevoPlan);
		clientesRegistrados.add(cliente);
	}
	
	public void registrarNuevoClientePlanNumerosAmigos(Cliente cliente, String planCliente, ArrayList<Integer> numerosAmigos) {
		IPlan nuevoPlan;
		nuevoPlan = factoria.getPlan(planCliente, numerosAmigos);
		cliente.setPlan(nuevoPlan);
		clientesRegistrados.add(cliente);
	}
	
	
	public Cliente buscarCliente(int numeroTelefonico) {
		for(Cliente cliente : clientesRegistrados)
		{
			if(cliente.getNumeroTelefonico() == numeroTelefonico)
				return cliente;
		}
		return null;
	}
	
	public void incluirNuevoRegistro(CDR registro) { 
		registrosTelefonicos.add(registro);
	}
	
	public void obtenerRegistrosTarifados() {
		for(CDR registro : registrosTelefonicos)
		{
			Cliente cliente = buscarCliente(registro.getNumeroTelefonoOrigen());
			registro.setCosto(tarifar(registro, cliente));
		}
	}

	public double tarifar(CDR registro, Cliente cliente) {
		return registro.calcularCostoDeLlamada(cliente.getPlan());
	}
	
	public void serializar() {
		try {
			FileOutputStream file = new FileOutputStream("test.txt");
			ObjectOutputStream out = new ObjectOutputStream(file);
			
			out.writeObject(registrosTelefonicos);
			
			out.close();
			file.close();
		} catch (IOException ex) {
			System.out.println("Exception caught");
		}
	}
	
	

}
