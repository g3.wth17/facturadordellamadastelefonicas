
public class CalculadorCostoLlamada {
	public double calcularCosto(String tipoCalculo, CDR registro, IPlan plan) {
		double tarifa = IPlan.tarifa.obtenerMontoTarifa(plan.getTipoTarifa(), registro);
		switch(tipoCalculo) {
		case "CALCULOSIMPLE": 
		{
			return tarifa * registro.convertirMinutosADecimal();
		}	
		default:
			return 0;
		}
		
	}

}
