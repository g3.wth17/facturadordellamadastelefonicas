
public class PlanPostpago implements IPlan{

	public double calcularCostoDeUnaLlamada(CDR registro) {
		return costoLlamada.calcularCosto("CALCULOSIMPLE", registro, this);
	}

	@Override
	public String getTipoTarifa() {
		return "POSTPAGO";
	}

}
