
public class Cliente {
	private String nombre, ci;
	private int numeroTelefonico;
	IPlan plan;
	
	Cliente(String nombre, String ci, int numeroTelefonico){
		this.nombre = nombre;
		this.ci = ci;
		this.numeroTelefonico = numeroTelefonico;
	}
	
	public void setNumeroTelefonico(int numeroTelefonico) {
		this.numeroTelefonico = numeroTelefonico;
	}
	
	public int getNumeroTelefonico() {
		return numeroTelefonico;
	}
	
	public IPlan getPlan() {
		return plan;
	}
	
	public void setPlan(IPlan plan) {
		this.plan = plan;
	}
}
