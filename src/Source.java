import java.util.ArrayList;

public class Source {

	public static void main(String[] args) {
		CentralTelefonica central = new CentralTelefonica();

		Cliente cliente1 = new Cliente("juanito","789",123);
		Cliente cliente2 = new Cliente("benito","788",456);
		Cliente cliente3 = new Cliente("anita","787",789);
		

		central.registrarNuevoClientePlanNormal(cliente1,"PREPAGO");
		central.registrarNuevoClientePlanNormal(cliente2,"POSTPAGO");
		
		ArrayList<Integer> numerosAuxiliares = new ArrayList<Integer>();
		numerosAuxiliares.add(234);
		numerosAuxiliares.add(345);
		numerosAuxiliares.add(456);
		numerosAuxiliares.add(567);
		central.registrarNuevoClientePlanNumerosAmigos(cliente3,"WOW", numerosAuxiliares);
			
		
		//CDR
		CDR registro1 = new CDR(123,456,"02:45", "sadf", "12:00");
		CDR registro2 = new CDR(789,123,"05:25", "sadf", "19:00");
		CDR registro3 = new CDR(456,789,"04:32", "sadf", "21:00");
		CDR registro4 = new CDR(456,123,"09:42", "sadf", "23:00");
		CDR registro5 = new CDR(789,123,"02:11", "sadf", "07:00");
		
		central.incluirNuevoRegistro(registro1);
		central.incluirNuevoRegistro(registro2);
		central.incluirNuevoRegistro(registro3);
		central.incluirNuevoRegistro(registro4);
		central.incluirNuevoRegistro(registro5);
		
		central.obtenerRegistrosTarifados();
		central.serializar();
		

	}

}
