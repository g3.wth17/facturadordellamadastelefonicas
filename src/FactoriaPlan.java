import java.util.ArrayList;

public class FactoriaPlan {
	IPlan getPlan(String tipoPlan) {
		if(esPrepago(tipoPlan)) 
			return new PlanPrepago();
		if(esPostpago(tipoPlan))
			return new PlanPostpago();
		return null;
	}
	
	IPlan getPlan(String tipoPlan,ArrayList<Integer> numerosAmigos) {
		if(esWow(tipoPlan))
			return new PlanWow(numerosAmigos);
		return null;
	}
	

	private boolean esWow(String tipoPlan) {
		return tipoPlan.equalsIgnoreCase("WOW");
	}

	private boolean esPostpago(String tipoPlan) {
		return tipoPlan.equalsIgnoreCase("POSTPAGO");
	}

	private boolean esPrepago(String tipoPlan) {
		return tipoPlan.equalsIgnoreCase("PREPAGO");
	}
	
	
	
}
