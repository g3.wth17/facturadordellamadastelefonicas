import java.util.ArrayList;

public class PlanWow implements IPlan{
	private ArrayList<Integer> numerosAmigos = new ArrayList<Integer>();
	PlanWow (ArrayList<Integer> numerosAmigos){
		this.numerosAmigos = numerosAmigos;
	}
	@Override
	public double calcularCostoDeUnaLlamada(CDR registro) {
		if(estaEntreNumerosAmigos(registro))
			return 0;
		else
			return costoLlamada.calcularCosto("CALCULOSIMPLE", registro, this);
	}

	private boolean estaEntreNumerosAmigos(CDR registro) {
		return numerosAmigos.contains(registro.getNumeroTelefonoDestino());
	}

	@Override
	public String getTipoTarifa() {
		return "WOW";
	}
	
}
